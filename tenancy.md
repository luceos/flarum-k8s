
Checklist for tenant functionality:

- identify tenant when using flarum binary
- identify tenant using request
- set database
- set cache
- set storage for avatars (core) and upload ext
- run install on new tenant
- run migrate on updates